import {createContext} from 'react'


export const GlobalContext = createContext({
    persons: [],
    modalActualizar: false,
    modalInsertar: false,
    form: {
      id: "",
      name: "",
      username: "",
      email: "",
    },

})

