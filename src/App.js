import React from 'react';
import './App.css';
import FormPerson from './components/FormPerson';
import { Container } from 'reactstrap';
import { BrowserRouter as Router ,useRoutes} from "react-router-dom";

function App() {

  const Rutas = () => {
    let routes = useRoutes([
      { path: "/", element: <FormPerson /> },
      // ...
    ]);
    return routes;
  };

  return (
    <div>
      <Container className='mp-3'>
        <Router>
          <Rutas />
        </Router>
      </Container>
    </div>

  );
}

export default App;
