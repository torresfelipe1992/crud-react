import { GlobalContext } from '../context/GlobalContext';
import { useEffect, useContext, useState} from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import React from 'react';
import axios from "axios";
import {
  Table,
  Button,
  Container,
  Modal,
} from "reactstrap";

export default function FormPerson() {

    const { form } = useContext(GlobalContext)
    console.log(form)

    const [persons, setPerson] = useState([])
    const baseURL = "https://jsonplaceholder.typicode.com/users";

    useEffect(() => {
        axios.get(baseURL).then((response) => {
            setPerson(response.data);
        });
    }, []);


     const  mostrarModalActualizar = (person) => {
        this.setPerson({
            form: person,
            modalActualizar: true,
        });
    }; 

    const  mostrarModalInsertar = () => {
        this.setPerson({
            modalInsertar: true,
        });
    };

    eliminar = (person) => {
        var opcion = window.confirm("Estás Seguro que deseas Eliminar el elemento " + person.id);
        if (opcion === true) {
          var contador = 0;
          var arreglo = persons;
          arreglo.map((registro) => {
            if (person.id === registro.id) {
              arreglo.splice(contador, 1);
            }
            contador++;
          });
          setPerson({ persons: arreglo, modalActualizar: false });
        }
      };
  

    return (
        <Container>
            <Button color="success" onClick={() => mostrarModalInsertar()}>Crear</Button>
            <br />
            <br />
            <Table striped bordered hover>
                <thead>
                    <tr>
                        <th>id</th>
                        <th>Name</th>
                        <th>Username</th>
                        <th>Email</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    {persons.map(person =>
                        <tr>
                            <td>{person.id}</td>
                            <td>{person.name}</td>
                            <td>{person.username}</td>
                            <td>{person.email}</td>
                            <td>
                                <Button
                                    color="primary"
                                    onClick={() => mostrarModalActualizar(person)}
                                >
                                    Editar
                                </Button>{" "}
                                <Button color="danger" onClick={() => eliminar(person)}>Eliminar</Button>
                            </td>
                        </tr>)}
                </tbody>
            </Table>
             <Modal/>           
        </Container>
    );
}

