import 'bootstrap/dist/css/bootstrap.min.css';
import React from 'react';
import axios from "axios";
import {
  Table,
  Button,
  Container,
  Modal,
  ModalHeader,
  ModalBody,
  FormGroup,
  ModalFooter,
} from "reactstrap";


export default class PersonList extends React.Component {
  state = {
    persons: [],
    modalActualizar: false,
    modalInsertar: false,
    form: {
      id: "",
      name: "",
      username: "",
      email: "",
    },
  }

  componentDidMount() {
    axios.get(`https://jsonplaceholder.typicode.com/users`)
      .then(res => {
        const persons = res.data;
        this.setState({ persons });
      })
  }

  mostrarModalActualizar = (person) => {
    this.setState({
      form: person,
      modalActualizar: true,
    });
  };

  cerrarModalActualizar = () => {
    this.setState({ modalActualizar: false });
  };

  mostrarModalInsertar = () => {
    this.setState({
      modalInsertar: true,
    });
  };

  cerrarModalInsertar = () => {
    this.setState({ modalInsertar: false });
  };

  editar = (person) => {
    var contador = 0;
    var arreglo = this.state.persons;
    arreglo.map((registro) => {
      if (person.id == registro.id) {
        arreglo[contador].name = person.name;
        arreglo[contador].username = person.username;
        arreglo[contador].email = person.email;
      }
      contador++;
    });
    this.setState({ persons: arreglo, modalActualizar: false });
  };

  eliminar = (person) => {
    var opcion = window.confirm("Estás Seguro que deseas Eliminar el elemento " + person.id);
    if (opcion == true) {
      var contador = 0;
      var arreglo = this.state.persons;
      arreglo.map((registro) => {
        if (person.id == registro.id) {
          arreglo.splice(contador, 1);
        }
        contador++;
      });
      this.setState({ persons: arreglo, modalActualizar: false });
    }
  };

  insertar = () => {
    var valorNuevo = { ...this.state.form };
    valorNuevo.id = this.state.persons.length + 1;
    var lista = this.state.persons;
    lista.push(valorNuevo);
    this.setState({ modalInsertar: false, persons: lista });
  }

  handleChange = (e) => {
    this.setState({
      form: {
        ...this.state.form,
        [e.target.name]: e.target.value,
      },
    });
  };

  render() {
    return (
      <>
        <Container>
          <Button color="success" onClick={() => this.mostrarModalInsertar()}>Crear</Button>
          <br />
          <br />
          <Table striped bordered hover>
            <thead>
              <tr>
                <th>id</th>
                <th>Name</th>
                <th>Username</th>
                <th>Email</th>
                <th>Acciones</th>
              </tr>
            </thead>
            <tbody>
              {this.state.persons.map(person =>
                <tr>
                  <td>{person.id}</td>
                  <td>{person.name}</td>
                  <td>{person.username}</td>
                  <td>{person.email}</td>
                  <td>
                    <Button
                      color="primary"
                      onClick={() => this.mostrarModalActualizar(person)}
                    >
                      Editar
                    </Button>{" "}
                    <Button color="danger" onClick={() => this.eliminar(person)}>Eliminar</Button>
                  </td>
                </tr>)}
            </tbody>
          </Table>
        </Container>

        <Modal isOpen={this.state.modalActualizar}>
          <ModalHeader>
            <div><h3>Editar Registro</h3></div>
          </ModalHeader>

          <ModalBody>
            <FormGroup>
              <label>
                Id:
              </label>

              <input
                className="form-control"
                readOnly
                type="text"
                value={this.state.form.id}
              />
            </FormGroup>

            <FormGroup>
              <label>
                Name:
              </label>
              <input
                className="form-control"
                name="name"
                type="text"
                onChange={this.handleChange}
                value={this.state.form.name}
              />
            </FormGroup>

            <FormGroup>
              <label>
                username:
              </label>
              <input
                className="form-control"
                name="username"
                type="text"
                onChange={this.handleChange}
                value={this.state.form.username}
              />
            </FormGroup>
            <FormGroup>
              <label>
                email:
              </label>
              <input
                className="form-control"
                name="email"
                type="text"
                onChange={this.handleChange}
                value={this.state.form.email}
              />
            </FormGroup>
          </ModalBody>

          <ModalFooter>
            <Button
              color="primary"
              onClick={() => this.editar(this.state.form)}
            >
              Editar
            </Button>
            <Button
              color="danger"
              onClick={() => this.cerrarModalActualizar()}
            >
              Cancelar
            </Button>
          </ModalFooter>
        </Modal>



        <Modal isOpen={this.state.modalInsertar}>
          <ModalHeader>
            <div><h3>Insertar usuario</h3></div>
          </ModalHeader>

          <ModalBody>
            <FormGroup>
              <label>
                Id:
              </label>

              <input
                className="form-control"
                readOnly type="text"
                value={this.state.persons.length + 1}
              />
            </FormGroup>

            <FormGroup>
              <label>
                name:
              </label>
              <input
                className="form-control"
                name="name"
                type="text"
                onChange={this.handleChange}
              />
            </FormGroup>

            <FormGroup>
              <label>
                username:
              </label>
              <input
                className="form-control"
                name="username"
                type="text"
                onChange={this.handleChange}
              />
            </FormGroup>
            <FormGroup>
              <label>
                email:
              </label>
              <input
                className="form-control"
                name="email"
                type="text"
                onChange={this.handleChange}
              />
            </FormGroup>
          </ModalBody>

          <ModalFooter>
            <Button
              color="primary"
              onClick={() => this.insertar()}
            >
              Insertar
            </Button>
            <Button
              className="btn btn-danger"
              onClick={() => this.cerrarModalInsertar()}
            >
              Cancelar
            </Button>
          </ModalFooter>
        </Modal>

      </>

    );
  }

}

