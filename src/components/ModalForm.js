import 'bootstrap/dist/css/bootstrap.min.css';
import React, { Fragment } from 'react';
import {
  Button,
  ModalHeader,
  ModalBody,
  FormGroup,
  ModalFooter,
} from "reactstrap";
import { Fragment } from 'react';

export default function Persona(props) {
  cerrarModalInsertar = () => {
    this.setPerson({ modalInsertar: false });
};
cerrarModalActualizar = () => {
    this.setPerson({ modalActualizar: false });
};

  editar = (person) => {
    var contador = 0;
    var arreglo = Person;
    arreglo.map((registro) => {
      if (person.id == registro.id) {
        arreglo[contador].name = person.name;
        arreglo[contador].username = person.username;
        arreglo[contador].email = person.email;
      }
      contador++;
    });
    this.setPerson({ Person: arreglo, modalActualizar: false });
  };

  

  insertar = () => {
    var valorNuevo = { ...this.form };
    valorNuevo.id = this.persons.length + 1;
    var lista = this.persons;
    lista.push(valorNuevo);
    this.setPerson({ modalInsertar: false, persons: lista });
  }

  handleChange = (e) => {
    this.setPerson({
      form: {
        ...this.form,
        [e.target.name]: e.target.value,
      },
    });
  };
  return (
    <Fragment>
      <Modal isOpen={this.state.modalActualizar}>
        <ModalHeader>
          <div><h3>Editar Registro</h3></div>
        </ModalHeader>

        <ModalBody>
          <FormGroup>
            <label>
              Id:
            </label>

            <input
              className="form-control"
              readOnly
              type="text"
              value={this.state.form.id}
            />
          </FormGroup>

          <FormGroup>
            <label>
              Name:
            </label>
            <input
              className="form-control"
              name="name"
              type="text"
              onChange={this.handleChange}
              value={this.state.form.name}
            />
          </FormGroup>

          <FormGroup>
            <label>
              username:
            </label>
            <input
              className="form-control"
              name="username"
              type="text"
              onChange={this.handleChange}
              value={this.state.form.username}
            />
          </FormGroup>
          <FormGroup>
            <label>
              email:
            </label>
            <input
              className="form-control"
              name="email"
              type="text"
              onChange={this.handleChange}
              value={this.state.form.email}
            />
          </FormGroup>
        </ModalBody>

        <ModalFooter>
          <Button
            color="primary"
            onClick={() => this.editar(this.state.form)}
          >
            Editar
          </Button>
          <Button
            color="danger"
            onClick={() => this.cerrarModalActualizar()}
          >
            Cancelar
          </Button>
        </ModalFooter>
      </Modal>



      <Modal isOpen={this.state.modalInsertar}>
        <ModalHeader>
          <div><h3>Insertar usuario</h3></div>
        </ModalHeader>

        <ModalBody>
          <FormGroup>
            <label>
              Id:
            </label>

            <input
              className="form-control"
              readOnly type="text"
              value={this.state.persons.length + 1}
            />
          </FormGroup>

          <FormGroup>
            <label>
              name:
            </label>
            <input
              className="form-control"
              name="name"
              type="text"
              onChange={this.handleChange}
            />
          </FormGroup>

          <FormGroup>
            <label>
              username:
            </label>
            <input
              className="form-control"
              name="username"
              type="text"
              onChange={this.handleChange}
            />
          </FormGroup>
          <FormGroup>
            <label>
              email:
            </label>
            <input
              className="form-control"
              name="email"
              type="text"
              onChange={this.handleChange}
            />
          </FormGroup>
        </ModalBody>

        <ModalFooter>
          <Button
            color="primary"
            onClick={() => this.insertar()}
          >
            Insertar
          </Button>
          <Button
            className="btn btn-danger"
            onClick={() => this.cerrarModalInsertar()}
          >
            Cancelar
          </Button>
        </ModalFooter>
      </Modal>

    </Fragment>
  )

}